import React from 'react';
import ReactDOM from 'react-dom';
import { MemoryRouter } from 'react-router';
import { Provider } from 'react-redux';
import { Home } from './';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(
    <MemoryRouter>
      <Home
        count={0}
        increment={() => {}}
        incrementAsync={() => {}}
        decrement={() => {}}
        decrementAsync={() => {}}
        changePage={() => {}}
      />
    </MemoryRouter>,
    div
  );
});
