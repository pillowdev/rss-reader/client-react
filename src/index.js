import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from './store';
import registerServiceWorker from './registerServiceWorker';
import App from './containers/app';
import withRoot from './withRoot';

const WithRootApp = withRoot(App);

render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <WithRootApp />
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
);

registerServiceWorker();
