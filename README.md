# rss-reader

## Build for Android

### Requirements

* android-sdk

```
export ANDROID_HOME=/opt/android-sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools:$ANDROID_HOME/tools/bin
```

* graddle

### Emulation

You first need to create a virtual device with android-sdk platform tools:

```
# Pull system from Google's servers
sdkmanager "system-images;android-26;google_apis;x86"
# Create device
avdmanager create avd -n Nexus5X_API26 -k "system-images;android-26;google_apis;x86" --tag "google_apis" --device "Nexus 5X"
```

### Debug

* `UnhandledPromiseRejectionWarning: TypeError: Cannot read property 'semver' of null`

Fixed in upstream https://github.com/apache/cordova-android/pull/422
Ensure that you have no avd with api 27.

```
avdmanager list avd
avdmanager delete avd -n "Nexus_5X_API_27_x86"
```

* `X Error of failed request: BadValue` when running cordova

```
LD_PRELOAD='/usr/$LIB/libstdc++.so.6' cordova emulate android
```

# TODO

Use ConnectedRouter from react-router-redux when merged (https://github.com/ReactTraining/react-router/pull/4717)
Setup error boundaries (https://fb.me/react-error-boundaries)
